#!/bin/bash

#
# xlsx2json
# By Jane Davis (https://gitlab.com/jane314/)
#

sudo apt update
sudo apt install gnumeric jq 
printf "Now place the files xlsx2json* from this Git repository in a common folder in your PATH variable, and follow the instructions here to install Deno: https://deno.land/.\n";
# xlsx2json

Convert `xlsx` files to `json` on an Ubuntu environment! Works great with WSL. 

The setup script should explain the install process. A few `apt` packages need to be installed, and all of the `xlsx2json*` files here should be placed in a common folder in your PATH variable.

This works by converting the `xlsx` file to an intermediate `csv` file. The `csv` file doesn't use escaped quotes or commas- it separates the table's columns with an uncommon UTF-8 character. By default 
"▞" is used. This character is configured in `xlsx2json-config.json`.


/**
 * xlsx2json
 * By Jane Davis (https://gitlab.com/jane314/)
 */

const output_file: string = Deno.args[0];

// Read and process CSV data
const csv_text_data: string = Deno.readTextFileSync('/tmp/xlsx2jsontmp.txt');
const csv_array_data: string[][] = csv_text_data.split('\n')
	.filter( line => !/^[[:space:]]*$/.test(line) )
	.map( line => line.split('▞')); 
const csv_array_data_head: string[] = csv_array_data[0];
const csv_array_data_tail: string[][] = csv_array_data.splice(1);

// Convert CSV data to JSON
const json_text_data: string = JSON.stringify(
	csv_array_data_tail.map( 
		row => row.reduce( 
			(acc: any, current_val: string, index: number) => {
				acc[csv_array_data_head[index]] = row[index];
				return acc;
			}, {} )));

// Write output file			
Deno.writeTextFileSync(output_file, json_text_data);

